#!/bin/bash

#Install EPEL Repo
wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ivh epel-release-6-8.noarch.rpm

#Install prerequisits
yum -y install dkms gcc-c++ make yum-utils kernel kernel-devel device-mapper git

#Install SRPMS centos source repo
#cat >> /etc/yum.repos.d/CentOS-Vault.repo <<END
#[base-SRPMS-6.6]
#name=CentOS-$releasever - Base SRPMS
#baseurl=http://vault.centos.org/6.6/os/Source
#gpgcheck=1
#gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
#priority=1
#enabled=1

#[updates-SRPMS-6.6]
#name=CentOS-$releasever - Base SRPMS
#baseurl=http://vault.centos.org/6.6/updates/Source
#gpgcheck=1
#gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
#priority=1
#enabled=1
#END

#yum -y update

#Download kernel source for making flashcache
#yumdownloader --source kernel.x86_64
#could also be at the following for openvz
wget http://download.openvz.org/kernel/branches/rhel6-2.6.32/042stab112.15/vzkernel-devel-2.6.32-042stab112.15.x86_64.rpm 
#(Note: Check the url and replace the file information to match your uname -r output!)
yum -y localinstall vzkernel-devel-2.6.32-042stab112.15.x86_64.rpm

#download flashcache and install it
git clone git://github.com/facebook/flashcache.git
cd flashcache
#make -f Makefile.dkms
make KERNEL_TREE=/usr/src/kernels/2.6.32-042stab112.15
make install KERNEL_TREE=/usr/src/kernels/2.6.32-042stab112.15
modprobe flashcache
