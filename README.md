installFlashCache
=================

Helper for installing FlashCache on CentOS6  
(How to install FlashCache)  

#  How to use this script  
1. Clone this repository with the following. 
 ```
git clone https://github.com/coreyman/installFlashCache
```
2. Change directory to the script. 
 ```
cd installFlashCache
```
3. Execute the bash script.
 ```
sh installFlashCache.sh
```

*Please report any bugs [here](https://github.com/coreyman/installFlashCache/issues)*

## What does this script do?

This script will install all the prerequisites and flashcache on Centos6.6. This script WILL NOT create your flashcache on the desired drives. You will need to unmount your source and desination drives. Run the appropriate flashcache_create command after this script has ran. Examples:  
``` flashcache_create -p thru <cachename> <SSD Dev> <DISK DEV> ```  
``` flashcache_create -p thru mysql_cache /dev/sdc1 /dev/mapper/vhdd-mysql ```  
``` flashcache_create -p around -s 220g -b 4k mycachedev /dev/md0 /dev/mapper/VolGrou-lv_vz ``` 
``` flashcache_create -p back ssd_cache /dev/md127 /dev/md0 ``` 
 Then run 
``` mount /dev/mapper/mycachedev /vz ```
``` You might have to unmount the destination first before you can load or create. Ex: umount /vz ```

Notes:  
After a reboot you will need to use flashcache_load ssd_devname [cachedev_name]
``` You might have to unmount the destination first before you can load or create. Ex: umount /vz ```
When you install a new kernel you will need to make and make install flashcache again.  
(After our last reboot on 10/20/15 flashcache with writeback had dirty blocks that it hadnt written to disk yet, and the cache device wouldnt load. The server was also hung at rebooting)
(This was us being dumb, we did a sysctl -w dev.flashcache.md127+md0.do_sync=1 on 11/7/2015 and all of the dirty blocks were written to disk before we restarted.) 
  

Cache Stats :
===========
Use 'dmsetup status' for cache statistics.  
  
'dmsetup table' also dumps a number of cache related statistics.
  
Examples :
dmsetup status cachedev  
dmsetup table cachedev  
  
Flashcache errors are reported in /proc/flashcache/<cache name>/flashcache_errors  
  
Flashcache stats are also reported in /proc/flashcache/<cache name>/flashcache_stats for easier parseability. 

Sysctl:
==========
sysctl -w dev.flashcache.md127+md0.max_clean_ios_set=8 ```quadruple default writes to hdds```
sysctl -w dev.flashcache.md127+md0.max_clean_ios_total=16 ```quadruple default writes to hdds```
sysctl -w dev.flashcache.md127+md0.skip_seq_thresh_kb=10240 ```Files greater than 10MB in size will go direct to HDDs``` 


See https://raw.githubusercontent.com/facebook/flashcache/master/doc/flashcache-sa-guide.txt for additional information.
